/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°2
 *
 * TestNumber.cpp
 * c++ -std=c++20 -o TestNumber Number.cpp TestNumber.cpp -lgtest
 */

#include <exception>
#include <sstream>
#include <utility>
#include <iostream>

#include <gtest/gtest.h>

#include "Number.hpp"


TEST(TestNumber, TestNumber0) {
    Number n{0};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}

TEST(TestNumber, TestNumber12345678) {
    Number n{12345678};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNumber, TestNumberBig) {
    Number n{1234512345};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "1234512345");
}

TEST(TestNumber, TestNumberConstruction) {
    Number n = Number{123456789};
    Number n2 = Number{n};

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "123456789");

    std::ostringstream os2;
    os2 << n2;
    EXPECT_EQ(os2.str(), "123456789");

    n = Number{12345};
    std::ostringstream os3;
    os3 << n;
    EXPECT_EQ(os3.str(), "12345");

    std::ostringstream os4;
    os4 << n2;
    EXPECT_EQ(os4.str(), "123456789");
}

TEST(TestNumber, TestNumberAffectation) {
    Number n = Number{123456789};
    Number n2 = n;

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "123456789");

    std::ostringstream os2;
    os2 << n2;
    EXPECT_EQ(os2.str(), "123456789");

    n = Number{12345};
    std::ostringstream os3;
    os3 << n;
    EXPECT_EQ(os3.str(), "12345");

    std::ostringstream os4;
    os4 << n2;
    EXPECT_EQ(os4.str(), "123456789");
}

TEST(TestNumber, TestNumberAddition0) {
    Number n = Number{123456789};
    n.add(0);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "123456789");
}

TEST(TestNumber, TestNumberAddition9999) {
    Number n = Number{9999};
    n.add(999999);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "1009998");
}

TEST(TestNumber, TestNumberAddition9) {
    Number n = Number{9999};
    n.add(9);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "10008");
}

TEST(TestNumber, TestNumberMultiply9) {
    Number n = Number{2111};
    n.multiply(9);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "18999");
}

TEST(TestNumber, TestNumberMultiply0) {
    Number n = Number{2111};
    n.multiply(0);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}

TEST(TestNumber, TestNumberMultiply1) {
    Number n = Number{2111};
    n.multiply(1);

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "2111");
}

TEST(TestNumber, TestFactorial123) {

    std::ostringstream os;

    os << factorial(123);;

    EXPECT_EQ(os.str(), "121463043670253296757662432418812958554542170884833823153289181618292"
                        "358923621676688311569606126402021707358352212940477825910915704116514"
                        "72186029519906261646730733907419814952960000000000000000000000000000");
}


TEST(TestNumber, TestNumberStringConstructor1) {
    try {
        Number{"11e223"};
        FAIL();
    }
    catch (const std::invalid_argument &expected) {}
}

TEST(TestNumber, TestNumberStringConstructor2) {
    Number n = Number{"2111"};

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "2111");
}

TEST(TestNumber, TestStream) {
    Number n = Number{0};
    std::stringstream ss;

    ss.str("123456");
    ss >> n;

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "123456");
}

TEST(TestNumber, TestAdditionOverload1) {
    Number n = Number{"1"};

    n += 2;

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "3");
}

TEST(TestNumber, TestAdditionOverload2) {
    Number n = Number{"111"};
    std::ostringstream os;
    os << n + 2;
    EXPECT_EQ(os.str(), "113");
}

TEST(TestNumber, TestMultiplicationOverload1) {
    Number n = Number{"11"};

    n *= 2;

    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "22");
}

TEST(TestNumber, TestMultiplicationOverload2) {
    Number n = Number{"11"};

    std::ostringstream os;
    os << n * 2;
    EXPECT_EQ(os.str(), "22");
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
