/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°2
 *
 * Number.cpp
 */

#include <exception>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

#include "Number.hpp"

Number::Number(unsigned long l) {
    first_ = new Digit{l % number_base, nullptr};
    Digit *current = first_;
    while (l > number_base) {
        l = l / number_base;
        current->next_ = new Digit{l % number_base, nullptr};
        current = current->next_;
    }
}

Number::Number(const Number &number) {
    first_ = new Digit{number.first_->digit_, nullptr};
    number.first_->duplicate(first_);
}

Number::Number(std::string s) {
    int number_of_digits = -1;

    for (int i = number_base; i > 0; i /= 10)number_of_digits += 1;

    for (int i = 0; i < s.length(); i += 1)if (!std::isdigit(s[i]))throw std::invalid_argument("Digits are required");

    first_ = new Digit{0, nullptr};
    first_->build(s, number_of_digits);
}

Number::~Number() {
    if (first_ != nullptr) {
        delete (first_);
    }
}

void Number::print(std::ostream &out) const {
    first_->print(out);
}

Number &Number::operator=(const Number &n) {
    if (this != &n) {
        delete (first_);
        first_ = new Digit{0, nullptr};
        n.first_->duplicate(first_);
    }
    return *this;
}

void Number::add(unsigned int i) {
    first_->add(i);
}

void Number::multiply(unsigned int i) {
    Number n = Number{*this};
    if (i <= 0) {
        delete (first_);
        first_ = new Digit{0, nullptr};
        return;
    }

    for (int j = 1; j < i; j++) {
        first_->add(n.first_, 0);
    }
}

Number factorial(unsigned int i) {
    Number n = 1;

    if (i <= 1) {
        return Number{1};
    }

    for (int j = 2; j <= i; j++) {
        n.multiply(j);
    }
    return n;
}


std::istream &operator>>(std::istream &in, Number &n) {
    in >> std::ws;
    std::string str = "";

    while (in.good()) {
        int c{in.get()};
        if (std::isdigit(c)) {
            unsigned int value{static_cast<unsigned int> (c - '0')};
            str.append(std::to_string(value));
        } else {
            in.putback(c);
            break;
        }
    }

    n = Number{str};
    return in;
}

Number &Number::operator*=(unsigned long n) {
    this->multiply(n);
    return *this;
}

Number &Number::operator+=(unsigned long n) {
    this->add(n);
    return *this;
}

Number operator*(const Number &n1, unsigned long n2) {
    return Number{n1} *= n2;
}

Number operator+(const Number &n1, unsigned long n2) {
    return Number{n1} += n2;
}

void Number::Digit::build(std::string s, int number_of_digits) {
    int len = number_of_digits < s.length() ? number_of_digits : s.length();
    int index = 0 > s.length() - number_of_digits ? 0 : s.length() - number_of_digits;
    int value = stoi(s.substr(index, len));
    int rest = value % number_base;
    int carry = value / number_base;
    digit_ += rest;
    if (carry != 0 or s.length() > number_of_digits) {
        next_ = new Digit{static_cast<DigitType>(carry), nullptr};
        next_->build(s.substr(0, s.length() - number_of_digits), number_of_digits);
    };
}

