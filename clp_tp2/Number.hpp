/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°2
 *
 * Number.hpp
 */

#ifndef NUMBER_HPP_INCLUDED
#define NUMBER_HPP_INCLUDED

#include <iostream>
#include <string>
#include <utility>

class Number {
public:
    Number(const Number &number);

    Number(unsigned long l);

    Number(std::string s);

    ~Number();

    void print(std::ostream &out) const;

    void add(unsigned int i);

    void multiply(unsigned int i);

    Number &operator=(const Number &n);

    Number &operator*=(unsigned long n);

    Number &operator+=(unsigned long n);

    friend Number operator*(const Number &n1, unsigned long n2);

    friend Number operator+(const Number &n1, unsigned long n2);

    friend std::istream &operator>>(std::istream &in, Number &n);

private:
    using DigitType = unsigned int;
    // Un seul chiffre décimal par maillon : l'objectif ici n'est pas la performance
    static const DigitType number_base{10u};

    struct Digit {
        DigitType digit_;
        Digit *next_;

        ~Digit() {
            if (next_ != nullptr) {
                delete (next_);
            }
        }

        void build(std::string s, int number_of_digits);

        void print(std::ostream &out) const {
            if (next_ != nullptr) {
                next_->print(out);
            }
            out << digit_;
        }

        void duplicate(Digit *digit) const {
            digit->digit_ = digit_;
            if (next_ != nullptr) {
                digit->next_ = new Digit{NULL, nullptr};
                next_->duplicate(digit->next_);
            }
        }

        void add(DigitType value) {

            if (value == 0) {
                return;
            }

            DigitType sum = digit_ + value;
            digit_ = sum % number_base;
            value = sum / number_base;
            if (next_ != nullptr) {
                next_->add(value);
            } else if (value != 0) {
                next_ = new Digit{0, nullptr};
                next_->add(value);
            }
        }

        void add(Digit *current, DigitType carry) {
            DigitType sum = digit_ + current->digit_ + carry;
            digit_ = sum % number_base;
            carry = sum / number_base;

            if (next_ != nullptr and current->next_ != nullptr) {
                next_->add(current->next_, carry);
            } else if (next_ != nullptr) {
                next_->add(carry);
            } else if (current->next_ != nullptr) {
                next_ = new Digit{0, nullptr};
                next_->add(current->next_, carry);
            } else if (carry != 0) {
                next_ = new Digit{carry, nullptr};
            }
        }
    };

    Digit *first_;
};

inline std::ostream &operator<<(std::ostream &out, const Number &n) {
    n.print(out);
    return out;
}

Number factorial(unsigned int i);

#endif
