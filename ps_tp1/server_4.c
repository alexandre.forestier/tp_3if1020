// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

#include <signal.h>


int running;

void stop_handler(int signo) {
    running = 0;
    printf("signal: %i\n", signo);
}

void exit_message() {
    printf("\nFin\n");
}

int main() {
    printf("Program has started successfully.\n");

    running = 1;

    int pipefd[2];

    int pdata = pipe(pipefd);

    if (pdata == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    atexit(exit_message);

    pid_t cpid = fork();

    if (cpid == -1) {
        printf("forkfailure");
        exit(EXIT_FAILURE);
    }

    if (cpid == 0) {
        printf("closed writing pipe for son\n");
        close(pipefd[1]);
        printf("Initialisation done son.\n");
        int buf;
        while (running == 1 && read(pipefd[0], &buf, sizeof(int)) > 0) {
            printf("fils\n");
            printf("%d\n",buf);
            printf("process père: %i\n", getppid());
            printf("process: %i\n", getpid());
        }
        printf("Fin du fils\n");
    } else {
        printf("closed reading pipe for father\n");
        close(pipefd[0]);
        printf("Initialisation done father.\n");
        while (running == 1) {

            int random_number = rand() % 100;

            printf("père\n");

            write(pipefd[1], &random_number, sizeof(int));
            printf("process père: %i\n", getppid());
            printf("process: %i\n", getpid());
            sleep(1);
        }
        printf("Fin du fils\n");
    }

    return EXIT_SUCCESS;
}
