/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * server_5.c
 */

// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int running;

void stop_handler(int signo) {
    running = 0;
    printf("signal: %i\n", signo);
}

void exit_message() {
    printf("\nFin\n");
}

int main() {
    printf("Beginning server init.\n");

    running = 1;

    char * myfifo = "named_pipe";

    // Creating the named file(FIFO)
    // mkfifo(<pathname>, <permission>)
    if(mkfifo(myfifo,0666)==-1){
        printf("error while creating pipe\n");
    }else{
        printf("created pipe\n");
    }

    int pipe = open(myfifo, O_WRONLY);

    if(pipe == -1){
        printf("pipe failure server\n");
    }

    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    atexit(exit_message);


    printf("server initialisation done\n");

    while(running==1){
        int random_number = rand() % 100;

        printf("server\n");

        write(pipe, &random_number, sizeof(int));
        printf("process père: %i\n", getppid());
        printf("process: %i\n", getpid());
        sleep(1);
    }

    close(pipe);

    return EXIT_SUCCESS;
}
