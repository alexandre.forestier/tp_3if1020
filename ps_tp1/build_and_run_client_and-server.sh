echo Building server_$1.c
gcc -o server_$1 server_$1.c
echo Building client_$1.c
gcc -o client_$1 client_$1.c
echo Launching ./server_$1
echo Launching ./client_$1
./server_$1 & (sleep 1 && ./client_$1)