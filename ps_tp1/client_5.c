/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * client_5.c
 */

// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

#include <signal.h>
#include <fcntl.h>


int running;

void stop_handler(int signo) {
    running = 0;
    printf("signal: %i\n", signo);
}

void exit_message() {
    printf("\nFin\n");
}

int main() {
    printf("Beginning client init.\n");

    running = 1;

    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    atexit(exit_message);

    char *pipename = "named_pipe";

    int pipe = open(pipename, O_RDONLY);

    if (pipe == -1) {
        printf("pipe failure client\n");
    }
    printf("Client initialisation done\n");

    int buf;

    while (running == 1 && read(pipe, &buf, sizeof(int)) > 0) {
        printf("client\n");

        printf("%d\n", buf);



        printf("process père: %i\n", getppid());
        printf("process: %i\n", getpid());
    }
    printf("Fin du fils\n");

    close(pipe);
    remove(pipename);

    return EXIT_SUCCESS;
}

