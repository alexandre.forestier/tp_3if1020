/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * server_2.c
 */

// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

#include <signal.h>


int running = 1;

void stop_handler(int signo)
{
    running = 0;
    printf("signal: %ld\n",signo);
}

void exit_message(){
    printf("Exiting");
}

int main() {
    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);

    printf("Program has started successfully.\n");

    atexit(exit_message);

    while (running == 1) {
        printf("process père: %ld\n", getppid());
        printf("process: %ld\n", getpid());
        printf("nombre: %ld\n", rand() % 100);
        sleep(1);
    }
    return EXIT_SUCCESS;
}

// utiliser la commande kill avec l'option -s INT (l'option doit être indiquée avant l'argument). Est-ce que le message a été affiché ?
// Oui => signal: 2
// Recommencer en utilisant la commande kill sans option. Est-ce que le message a été affiché ?
// Non => [1]    14108 terminated  ./server_2
//Modifier votre programme pour que le message soit affiché dans ce cas.
// => On rajoute un sigTerm

//Utiliser la commande kill avec l'option -s KILL (plus connue sous le nom kill -9). Est-ce que le message a été affiché ?
// => Non
// Peut-on faire en sorte qu'il le soit ?
// => Non la dicumentation dit `signum specifies the signal and can be any valid signal except
//       SIGKILL and SIGSTOP.`
// Que se passe-t'il si vous donnez comme argument à la commande kill (sans, puis avec l'option -s KILL) le numéro du processus père ?
//  Dans le premier cas rien dans le deuxième cela ferme le shell executant le programme

//Modifier la fonction stop_handler() afin de ne plus modifier la variable running. Est-ce que maintenant votre programme s'arrête via un
// CTRL-C ? => non la première fois (grace stopping) oui la deuxième (forced stop)
// un kill ? => non
// un kill -9 ? => oui

//Atexit :
// CTRL-C : Appelée
// un kill : Appelée
// un kill -9 : Non appelée