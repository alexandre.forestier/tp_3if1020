/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * server_1.c
 */

// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

int main()
{
    printf("Program has started successfully.\n");

    while (1){
        printf("process père: %ld\n",getppid());
        printf("process: %ld\n",getpid());
        printf("nombre: %ld\n",rand()%100);
        sleep(1);
    }
    return EXIT_SUCCESS;
}

