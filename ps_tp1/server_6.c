/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * server_6.c
 */

#include<stdio.h>
#include<sys/socket.h>
#include<arpa/inet.h>	//inet_addr
#include<unistd.h>	//write
#include <stdlib.h>
#include <signal.h>

#define PORT 9600

int running;

void stop_handler(int signo) {
    running = 0;
    printf("signal: %i\n", signo);
}

void exit_message() {
    printf("\nFin\n");
}

int main()
{
    running = 1;
    int socket_desc , client_sock , c ;
    struct sockaddr_in server , client;
    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    atexit(exit_message);

    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( PORT );

    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");


    listen(socket_desc , 3);

    printf("Socket initialized waiting for a remote connection...");
    c = sizeof(struct sockaddr_in);

    //accept connection from an incoming client
    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_sock < 0)
    {
        printf("accept failed\n");
        return 1;
    }
    printf("Connection accepted\n");

    //Receive a message from client
    while( running == 1 )
    {
        sleep(1);
        int number = rand()%100;
        write(client_sock , &number , sizeof(int));
        printf("[server] process père: %i\n", getppid());
        printf("[server] process: %i\n", getpid());
        sleep(1);
    }

    return 0;
}