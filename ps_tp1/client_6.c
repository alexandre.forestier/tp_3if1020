/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * client_6.c
 */

#include <stdio.h>	//printf
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#define PORT 9600

int running;

void stop_handler(int signo) {
    running = 0;
    printf("signal: %i\n", signo);
}

void exit_message() {
    printf("\nFin\n");
}

int main()
{
    int sock;
    struct sockaddr_in socketc;

    running = 1;
    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
    atexit(exit_message);

    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket\n");
    }

    socketc.sin_addr.s_addr = inet_addr("127.0.0.1");
    socketc.sin_family = AF_INET;
    socketc.sin_port = htons(PORT );

    //Connect to remote socket
    if (connect(sock , (struct sockaddr *)&socketc , sizeof(socketc)) < 0)
    {
        printf("connect failed. Error\n");
        return 1;
    }

    puts("Connected\n");

    //keep communicating with socket
    while(running == 1)
    {
        int buf;
        if( recv(sock , &buf , sizeof(int) , 0) < 0)
        {
            printf("failed to receive\n");
            break;
        }
        printf("[client] process père: %i\n", getppid());
        printf("[client] process: %i\n", getpid());
        sleep(1);

        printf("Received %i\n",buf);
    }

    close(sock);
    return 0;
}