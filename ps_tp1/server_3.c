/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmation système - TP n°1
 *
 * server_3.c
 */

// for printf()
#include <stdio.h>
// For rand(), srand(), sleep(), EXIT_SUCCESS
#include <stdlib.h>
// For time()
#include <time.h>
// For getpid(), getppid()
#include <unistd.h>

#include <signal.h>


int running = 1;

void stop_handler(int signo)
{
    running = 0;
    printf("signal: %i\n",signo);
}

void exit_message(){
    printf("Exiting");
}

int main() {
    struct sigaction sa;
    sa.sa_sigaction = &stop_handler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);

    printf("Program has started successfully.\n");

    atexit(exit_message);

    int cpid = fork();
    if (cpid == -1) {
        exit(EXIT_FAILURE);
    }
    srand(cpid + clock());

    while (running == 1) {
        if(cpid == 0){
            printf("fils\n");
        }else{
            printf("père\n");
        }
        printf("process père: %i\n", getppid());
        printf("process: %i\n", getpid());
        printf("nombre: %i\n", rand() % 100);
        printf("\n");
        sleep(1);
    }

    if(cpid!=0){
        wait(&cpid);
    }

    return EXIT_SUCCESS;
}

// Comment peut-on distinguer les messages du père de ceux du fils ? => père: un process père + un process id (P0), fils: un process père (P0) + un un id de process
// Est-ce que les 2 processus s'arrêtent avec CTRL-C ? => Oui
// Utiliser kill pour arrêter le processus fils puis ps a de nouveau : que remarquez-vous ?
//  => il s'est arrêté mais est tjrs dans la listes des processus
// Tuer le père, quels sont les changements visibles ?
// => Les process ont bien été clean
