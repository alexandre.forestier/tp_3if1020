/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°1
 *
 * tris.cpp
 */

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

#include <cstdlib>
#include <ctime>


void print_tab(const std::vector<int> tab) {
    std::cout << " [ ";
    for (int i = 0; i < tab.size(); i++) {
        std::cout << tab[i] << " ";
    }
    std::cout << " ]" << std::endl;
}

void test_11() {
    std::cout << "test11" << std::endl;
    const std::vector<int> tab{1, -2, 3, -4, 5, -6};
    print_tab(tab);
}

void random_tab(std::vector<int> &tab) {
    for (int i = 0; i < tab.size(); i += 1) {
        tab[i] = rand() % 21 - 10;
    }
}

void test_12() {
    std::cout << "test12" << std::endl;
    std::vector<int> tab(10);
    print_tab(tab);
    random_tab(tab);
    print_tab(tab);
}

void bubble_sort(std::vector<int> &tab) {
    for (int i = 0; i < tab.size() - 1; i++) {
        for (int j = 0; j < tab.size() - i - 1; j++) {
            if (tab[j] > tab[j + 1]) {
                std::swap(tab[j], tab[j + 1]);
            }
        }
    }
};

void test_13() {
    std::cout << "test13" << std::endl;
    std::vector<int> tab(10);
    print_tab(tab);
    random_tab(tab);
    print_tab(tab);
    bubble_sort(tab);
    print_tab(tab);
}

bool less(int a, int b) {
    return a < b;
}

bool greater(int a, int b) {
    return a > b;
}

void bubble_sort_2(std::vector<int> &tab, bool func(int, int)) {
    for (int i = 0; i < tab.size() - 1; i++) {
        for (int j = 0; j < tab.size() - i - 1; j++) {
            if (func(tab[j], tab[j + 1])) {
                std::swap(tab[j], tab[j + 1]);
            }
        }
    }
};

void test_14() {
    std::cout << "test14 greater" << std::endl;
    std::vector<int> tab(10);
    print_tab(tab);
    random_tab(tab);
    print_tab(tab);
    bubble_sort_2(tab, greater);
    print_tab(tab);

    std::cout << "test14 less" << std::endl;
    bubble_sort_2(tab, less);
    print_tab(tab);
}

void bubble_sort_3(std::vector<int> &tab, std::function <bool(int, int)> func) {
    for (int i = 0; i < tab.size() - 1; i++) {
        for (int j = 0; j < tab.size() - i - 1; j++) {
            if (func(tab[j], tab[j + 1])) {
                std::swap(tab[j], tab[j + 1]);
            }
        }
    }
};

void test_15() {
    std::cout << "test15 greater" << std::endl;
    std::vector<int> tab(10);
    print_tab(tab);
    random_tab(tab);
    print_tab(tab);
    bubble_sort_3(tab, [](int a, int b){return abs(a) > abs(b);});
    print_tab(tab);

    std::cout << "test15 less" << std::endl;
    bubble_sort_3(tab, [](int a, int b){return abs(a) < abs(b);});
    print_tab(tab);
}

int main() {
    test_11();
    test_12();
    test_13();
    test_14();
    test_15();
    std::srand(std::time(nullptr));

    return 0;
}

