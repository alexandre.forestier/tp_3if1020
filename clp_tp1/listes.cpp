/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°1
 *
 * listes.cpp
 */

#include <iostream>
#include <forward_list>
#include <functional>
#include <limits>

#include <cstdlib>
#include <ctime>

std::forward_list< int > random_list(int length){
    std::forward_list< int > list;
    for(int i=0; i < length; i++){
        list.push_front(rand()%100);
    }
    return list;
}

void print_list(const std::forward_list< int > liste){
    std::cout << "[";
    for( auto  i : liste ) {
        std::cout<<" "<<i;
    };
    std::cout << " ]" << std::endl;
}

void test_21(){
    std::cout << "test21" << std::endl;
    print_list(random_list(10));
}

std::forward_list< int > map_iter(const std::forward_list<int> list, std::function<int(int)> func){
    std::forward_list< int > mapped_list;
    for( auto  i : list ) {
        mapped_list.push_front(func(i));
    };
    return mapped_list;
}

void test_22(){
    std::cout << "test22" << std::endl;
    std::forward_list< int > liste = random_list(10);
    print_list(liste);
    print_list(map_iter(liste, [](int x) {return 3 * x;}));
}

std::forward_list< int > filter_iter(const std::forward_list<int> list, bool func(int)){
    std::forward_list< int > mapped_list;
    for( auto  i : list ) {
        if(func(i)){
            mapped_list.push_front(i);
        }
    }
    return mapped_list;
}

void test_23(){
    std::cout << "test23" << std::endl;
    std::forward_list< int > liste = random_list(10);
    print_list(liste);
    std::forward_list< int > liste_2 = map_iter(liste, [](int x) {return 3 * x;});
    print_list(liste_2);
    print_list(filter_iter(liste_2, [](int x) {return (x%2==1)?false:true ;}));
}

void test_24(){
    std::cout << "test24" << std::endl;
    int coef = rand()%5 + 1;
    std::cout << "coeff : "<< coef << std::endl;
    std::forward_list< int > liste = random_list(10);
    print_list(liste);
    std::forward_list< int > liste_2 = map_iter(liste, [coef](int x) {return coef * x;});
    print_list(liste_2);
    print_list(filter_iter(liste_2, [](int x) {return (x%2==1)?false:true ;}));
}

int reduce(const std::forward_list<int> list, int u0, int func(int, int)){
    int value = u0;
    for( auto  i : list ) {
        value = func(value,i);
    };
    return value;
}

void test25(){
    std::cout << "test25" << std::endl;
    std::forward_list<int> list = random_list(10);
    print_list(list);
    std::cout << "min " << reduce(list,99,[](int a, int b){return (a<b)?a:b;}) << std::endl;
    std::cout << "max " << reduce(list,0,[](int a, int b){return (a>b)?a:b;}) << std::endl;
}

int fold_left_aux(std::forward_list< int >::const_iterator it,std::forward_list< int >::const_iterator itEnd,int u0, int func(int, int)){
    if(it == itEnd){
        return u0;
    }else{
        int value = *it;
        return fold_left_aux(++it,itEnd,func(u0,value),func);
    }
}

int fold_left(const std::forward_list<int> list,int u0, int func(int, int)){
    return fold_left_aux(list.cbegin(),list.cend(),u0,func);
}

void test31(){
    std::cout << "test31" << std::endl;
    std::forward_list<int> list = random_list(10);
    print_list(list);
    std::cout << "min " << fold_left(list,99,[](int a, int b){return (a<b)?a:b;}) << std::endl;
    std::cout << "max " << fold_left(list,0,[](int a, int b){return (a>b)?a:b;}) << std::endl;
}

std::forward_list<int> map_left_aux(std::forward_list< int >::const_iterator it,std::forward_list< int >::const_iterator itEnd, std::function<int(int)> func){
    std::forward_list< int > mapped_list;
    if(it == itEnd){
        std::forward_list< int > list;
        return list;
    }else{
        int value = *it;
        std::forward_list< int > list = map_left_aux(++it,itEnd,func);

        list.push_front(func(value));

        return list;
    }
}

std::forward_list<int> map_left(const std::forward_list<int> list, std::function<int(int)> func){
    return map_left_aux(list.cbegin(),list.cend(),func);
}

std::forward_list<int> filter_left_aux(std::forward_list< int >::const_iterator it,std::forward_list< int >::const_iterator itEnd, std::function<bool(int)> func){
    std::forward_list< int > mapped_list;
    if(it == itEnd){
        std::forward_list< int > list;
        return list;
    }else{
        int value = *it;
        std::forward_list< int > list = filter_left_aux(++it,itEnd,func);

        if(func(value)){
            list.push_front(value);
        }
        return list;
    }
}

std::forward_list<int> filter_left(const std::forward_list<int> list, bool func(int)){
    return filter_left_aux(list.cbegin(),list.cend(),func);
}

void test_32(){
    std::cout << "test32" << std::endl;
    int coef = rand()%5 + 1;
    std::cout << "coeff : "<< coef << std::endl;
    std::forward_list< int > liste = random_list(10);
    print_list(liste);
    std::forward_list< int > liste_2 = map_left(liste, [coef](int x) {return coef * x;});
    print_list(liste_2);
    print_list(filter_left(liste_2, [](int x) {return (x%2==1)?false:true ;}));
}

void test_33(){
    std::cout << "test33" << std::endl;
    int coef = rand()%5 + 1;
    std::cout << "coeff : "<< coef << std::endl;
    std::forward_list< int > liste = random_list(10);
    print_list(liste);
    std::forward_list< int > liste_2 = map_left(liste, [coef](int x){auto mult = std::multiplies<int>(); return mult(coef,x);});
    print_list(liste_2);
    print_list(filter_left(liste_2, [](int x) {return (x%2==1)?false:true ;}));
}

int main()
{
    std::srand( std::time( nullptr ));

    test_21();
    test_22();
    test_23();
    test_24();
    test25();
    test31();
    test_32();
    test_33();

    return 0;
}
