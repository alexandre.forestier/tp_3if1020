/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmations système - TP n°2
 *
 * MessageBox.hpp
 */

#pragma once

#include <array>
#include <algorithm>
#include <mutex>
#include <condition_variable>

#include "../BasicMessageBox.hpp"

/*
 * FIFO d'echange de messages entre producteurs et consommateurs
 * Version pour synchronisation entre threads
 */
class MessageBox : public BasicMessageBox {
public:
    void put( int message ) {
        // TODO : ajouter les mecanismes de synchronisation
        std::unique_lock<std::mutex> lock(mutex);

        if (nb_msg_ == box_size_)
        {
            free_space.wait(lock);
        }

        basic_put( message );
        ++nb_msg_;
        lock.unlock();
        msg.notify_one();
    }
 
    int get() {
        // TODO : ajouter les mecanismes de synchronisation
        std::unique_lock<std::mutex> lock(mutex);

        if(nb_msg_ == 0){
            msg.wait(lock);
        }

        int message{ basic_get() };
        --nb_msg_;
        free_space.notify_one();

        return message;
    }
private:
    // TODO : ajouter les objets de synchronisation
    std::mutex mutex;
    std::condition_variable msg;
    std::condition_variable free_space;
    int nb_msg_ = 0;

};
 
