/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Programmations système - TP n°2
 *
 * Threads.cpp
 * c++ -std=c++20 Threads.cpp -o Threads -lpthread
 */

#include <iostream>
#include <sstream>
#include <thread>
 
 
#include "../Random.hpp"
#include "../osyncstream.hpp"

#include "Producer.hpp"
#include "Consumer.hpp"
#include "MessageBox.hpp"
 
/*
 * Test avec 1 producteur et 1 consommateur
 */
int main() {
    // Créer un générateur de nombres aléatoires
    // Créer une boîte à lettres
    // Créer un producteur et un consommateur
    // Créer les threads correspondants
    // Attendre la fin des threads

    Random random{50};
    MessageBox messageBox;

    std::vector<std::thread> ConsOrProdThreads;

    for(unsigned int i = 0; i < 4; i++)ConsOrProdThreads.push_back(std::thread{ Producer {i,messageBox,random,10}});
    for(unsigned int i = 0; i < 2; i++)ConsOrProdThreads.push_back(std::thread{ Consumer {i,messageBox,random,20}});
    for(unsigned int i = 0; i < ConsOrProdThreads.size(); i++) ConsOrProdThreads[i].join();

    return 0;
}

