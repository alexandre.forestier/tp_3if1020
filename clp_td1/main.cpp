#include <iostream>
#include <math.h>
#include <numbers>
#include <string>



double sin_x_plus_cos_sqrt2_times_x( double x ){
    return sin(x) + cos(std::numbers::sqrt2 * x);
}

void test11(){
    std::cout << sin_x_plus_cos_sqrt2_times_x(1) <<std::endl;
    std::cout << sin_x_plus_cos_sqrt2_times_x(-4.5) <<std::endl;
};

void test12(){
    std::string value;
    std::cout << "Please enter your value: ";
    std::cin >> value;

    float num_value = std::stof(value);

    std::cout << sin_x_plus_cos_sqrt2_times_x(num_value) <<std::endl;
};

void test21(){
    std::string value;
    std::cout << "Please enter your value: ";
    std::cin >> value;

    float num_value = std::stof(value);

    for(int i = 0; i < 10; i ++){
        float real_value = num_value + i;
        std::cout<< "sin_x_plus_cos_sqrt2_times_x( "<< real_value << ") = "<< sin_x_plus_cos_sqrt2_times_x(real_value) <<std::endl;
    }

    std::cout << sin_x_plus_cos_sqrt2_times_x(num_value) <<std::endl;
};

void print_sin_x_plus_cos_sqrt2_times_x( double begin, double end, double step )
{
    if (end < begin || step < 0){
        std::cout << "You should lear how to count" <<std::endl ;
        return;
    }

    for(int i = begin; i <= end; i += step){

        std::cout<< "sin_x_plus_cos_sqrt2_times_x( "<< i << ") = "<< sin_x_plus_cos_sqrt2_times_x(i) <<std::endl;
    }
}

void test22(){
    print_sin_x_plus_cos_sqrt2_times_x(-10,10,2);
}

void print_sin_x_plus_cos_sqrt2_times_x(){
    while(true) {
        std::string lowerBound;
        std::cout << "Please enter your lower boundary: ";
        std::cin >> lowerBound;
        float castedLowerBound = std::stof(lowerBound);

        std::string upperBound;
        std::cout << "Please enter your upper boundary: ";
        std::cin >> upperBound;
        float castedUpperBound = std::stof(upperBound);


        std::string stepNumber;
        std::cout << "Please enter your step number: ";
        std::cin >> stepNumber;
        int castedStepNumber = std::stoi(stepNumber);

        if (lowerBound >= upperBound){
            std::cout << "lower bound should be smaller than upper bound" << std::endl;
        }else if(castedStepNumber<2){
            std::cout << "you should have at least 2 steps" << std::endl;
        }
        else {
            float increment = (castedUpperBound - castedLowerBound)/(castedStepNumber-1);
            for (float i = castedLowerBound; i <= castedUpperBound; i += increment) {
                std::cout << "sin_x_plus_cos_sqrt2_times_x( " << i << ") = "<< sin_x_plus_cos_sqrt2_times_x(i) << std::endl;
            }
            break;
        }

    }
}

double compute_derivative( double func( double ), double x, double epsilon ){
    return (func(x+epsilon)-func(x))/epsilon;
};

void test31(){
    for(double i = -4.6; i <=-4.5; i+=0.01){
        std::cout << "derivative of sin_x_plus_cos_sqrt2_times_x( ";
        std::cout << i ;
        std::cout << " ) = ";
        std::cout << compute_derivative(sin_x_plus_cos_sqrt2_times_x, i, 0.00001) << std::endl;
    }
}

double find_zero( double func( double ), double begin, double end, double precision ){
    double x = (begin+end)/2;

    if((begin+end)/2 == begin || (begin+end)/2 == end){
        return INFINITY;
    }

    if(abs(func(x)) < precision){
        return x;
    }else if(func(x) > 0){
        return find_zero(func,begin,x, precision);
    }else{
        return find_zero(func,x,end, precision);
    }
}

void test32(){
    std::cout << find_zero(sin_x_plus_cos_sqrt2_times_x,-2,0,0.00001)<<std::endl;
}

int find_all_zeros( double func( double ), double begin, double end, double width,
                    double precision, double results[], int max_number_of_results ){
    int index = 0;
    for(double minInterval = begin; minInterval < end; minInterval += width ){
        double maxInterval = minInterval + width;
        double candidate = find_zero(func,minInterval,maxInterval,precision);
        std::cout << " " << minInterval << " " << maxInterval << " " << width << " " << candidate << std::endl;
        if(candidate != INFINITY){
            results[index]=candidate;
            index += 1;
            if(false){
                std::cout << "yo" << std::endl;
                return index;
            }
        }
    }

    return index;
}

void test41(){
    double tab[10] {0};

    std::cout << find_all_zeros(sin_x_plus_cos_sqrt2_times_x,-10,10,0.5,0.00001, tab,10) << std::endl;

    for (size_t i = 0; i < 10; i++)
    {
        std::cout << tab[i] << " ";
    }
}

int find_all_extrema( double func( double ), double begin, double end, double width,
                      double precision, double epsilon, double results[], int max_number_of_results ){
    int index = 0;
    for(double minInterval = begin; minInterval < end; minInterval += width ){
        double maxInterval = minInterval + width;
        double candidate = find_zero(func,minInterval,maxInterval,precision);
        std::cout << " " << minInterval << " " << maxInterval << " " << width << " " << candidate << std::endl;
        if(candidate != INFINITY){
            results[index]=candidate;
            index += 1;
            if(false){
                std::cout << "yo" << std::endl;
                return index;
            }
        }
    }

    return index;
}

void test42(){
    double tab[10] {0};

    std::cout << find_all_zeros(sin_x_plus_cos_sqrt2_times_x,-10,10,0.5,0.00001, tab,10) << std::endl;

    for (size_t i = 0; i < 10; i++)
    {
        std::cout << tab[i] << " ";
    }
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    //test11();
    //test12();
    //test21();
    //test22();
    //print_sin_x_plus_cos_sqrt2_times_x();
    //test31();
    test32();
    test41();
    return 0;
}


