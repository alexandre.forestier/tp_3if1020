//
// Created by alexf on 02/11/2021.
//

#ifndef CLP_TP3_NOMBRE_HPP
#define CLP_TP3_NOMBRE_HPP

#endif //CLP_TP3_NOMBRE_HPP

#include "Expression.hpp"
#include <format>

class Nombre : public Expression {

public:
    Nombre(int int_val) {
        ++nb_instance;
        value = int_val;
    }

    ~Nombre() {
        --nb_instance;
    }

    void afficher(std::ostream &out) const override {
        out << value;
    };

    Expression *derive(std::string nom) const override {
        return new Nombre{0};
    }

    Expression *clone() const override {
        return new Nombre{value};
    }

    Expression *simplify() const {
        return new Nombre{value};
    }

    std::string type() const override {
        return "Num";
    }


    static int get_nb_instance() {
        return nb_instance;
    }

private:
    int value;
    static int nb_instance;

};

int Nombre::nb_instance{0};
