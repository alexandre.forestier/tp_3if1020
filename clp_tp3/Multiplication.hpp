//
// Created by alexf on 16/11/2021.
//

#ifndef CLP_TP3_MULTIPLICATION_HPP
#define CLP_TP3_MULTIPLICATION_HPP

#include "Operation.hpp"
#include "Expression.hpp"


class Multiplication : public Operation {
public:
    Multiplication(Expression *lhs, Expression *rhs) {
        this->lhs = lhs;
        this->rhs = rhs;
        this->op = "*";
    };

    Expression *derive(std::string nom) const {
        Expression *u = lhs->clone();
        Expression *v = rhs->clone();

        return new Addition(new Multiplication(rhs->derive(nom), u), new Multiplication(lhs->derive(nom), v));
    };

    Expression *clone() const {
        return new Multiplication(lhs->clone(), rhs->clone());
    };

    Multiplication *cloneMult() const {
        return new Multiplication(lhs->clone(), rhs->clone());
    };

    Expression *simplify() const {
        Multiplication *temp = cloneMult();
        return temp->simpl();
    }

    std::string type() const override {
        return "Mul";
    }

private:
    Expression *simpl() const {
        lhs->simplify();
        rhs->simplify();

        if (lhs->type() == "Num") {
            std::ostringstream os;
            os << *lhs;

            if (os.str() == "1") {
                return rhs;
            }
            if (os.str() == "0") {
                return new Nombre{0};
            }
        }

        if (rhs->type() == "Num") {
            std::ostringstream os;
            os << *rhs;
            if (os.str() == "1") {
                return lhs;
            }

            if (os.str() == "0") {
                return new Nombre{0};
            }
        }
        return (Expression *) this;
    };
};

#endif //CLP_TP3_MULTIPLICATION_HPP