/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°3
 *
 * Expression.hpp
 */

#ifndef EXPRESSION_HPP_INCLUDED
#define EXPRESSION_HPP_INCLUDED

#include <iostream>
#include <string>
#include <utility>
#include <memory>

class Expression {
public:
    virtual Expression *derive(std::string nom) const = 0;

    virtual void afficher(std::ostream &out) const = 0;

    virtual Expression *clone() const = 0;

    virtual Expression *simplify() const = 0;

    virtual ~Expression() = default;

    virtual std::string type() const = 0;

private:
};

std::ostream &operator<<(std::ostream &out, Expression &expr) {
    expr.afficher(out);
    return out;
}

#endif
