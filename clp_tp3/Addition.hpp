//
// Created by alexf on 15/11/2021.
//

#ifndef CLP_TP3_ADDITION_H
#define CLP_TP3_ADDITION_H

#include "Operation.hpp"

class Addition : public Operation {
public:
    Addition(Expression *lhs, Expression *rhs) {
        this->lhs = lhs;
        this->rhs = rhs;
        this->op = "+";
    };

    Expression *derive(std::string nom) const {
        return new Addition(lhs->derive(nom), rhs->derive(nom));
    };

    Expression *clone() const {
        return new Addition(lhs->clone(), rhs->clone());
    };

    Expression *simplify() const {

        if (lhs->type() == "Num") {
            std::ostringstream os;
            os << *lhs;

            if (os.str() == "0") {
                return rhs;
            }
        }

        if (rhs->type() == "Num") {
            std::ostringstream os;
            os << *rhs;
            if (os.str() == "0") {
                return lhs;
            }
        }

        return this->clone();
    }

    std::string type() const override {
        return "Add";
    }
};

#endif //CLP_TP3_ADDITION_H