//
// Created by alexf on 15/11/2021.
//

#ifndef CLP_TP3_OPERATION_H
#define CLP_TP3_OPERATION_H

#include "Expression.hpp"

class Operation : public Expression {
public:
    void afficher(std::ostream &out) const {
        out << *lhs << " " << op << " " << *rhs;
    }

    virtual ~Operation() {
        delete lhs;
        delete rhs;
    }

    std::string op;
protected:
    Expression *lhs;
    Expression *rhs;
};

std::ostream &operator<<(std::ostream &out, Operation &expr) {
    expr.afficher(out);
    return out;
}

#endif //CLP_TP3_OPERATION_H