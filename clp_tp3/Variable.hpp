//
// Created by alexf on 02/11/2021.
//

#ifndef CLP_TP3_VARIABLE_HPP
#define CLP_TP3_VARIABLE_HPP

#endif //CLP_TP3_VARIABLE_HPP

#include <string>
#include "Expression.hpp"


class Variable : public Expression {
public:
    Variable(std::string name) {
        this->name = name;
        ++nb_instance;
    }

    ~Variable() {
        --nb_instance;
    }

    Expression *derive(std::string nom) const override {
        return nom == name ? new Nombre{1} : new Nombre{0};
    }

    void afficher(std::ostream &out) const override {
        out << name;
    };

    static int get_nb_instance() {
        return nb_instance;
    }

    std::string type() const override {
        return "Var";
    }


    Expression *clone() const override {
        return new Variable{name};
    }

    Expression *simplify() const override {
        return clone();
    }

private:
    std::string name;
    static int nb_instance;
};

int Variable::nb_instance{0};