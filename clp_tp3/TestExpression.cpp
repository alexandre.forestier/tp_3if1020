/*
 * Cursus CentraleSupélec - Dominante Informatique et numérique
 * 3IF1020 - Concepts des langages de programmation - TP n°3
 *
 * TestExpression.cpp
 * c++ -std=c++20 -o TestExpression TestExpression.cpp Expression.cpp -lgtest -lpthread
 */

#include <sstream>
#include <utility>

#include <gtest/gtest.h>

#include "Expression.hpp"
#include "Nombre.hpp"
#include "Variable.hpp"
#include "Addition.hpp"
#include "Multiplication.hpp"

TEST(TestVariable, TestIntanciation) {
    Variable v{"x"};
    std::ostringstream os;
    os << v;
    EXPECT_EQ(os.str(), "x");

}

TEST(TestVariable, DerivationSelf) {
    Variable v{"x"};
    std::ostringstream os;
    Expression *n = v.derive("x");
    os << *n;
    EXPECT_EQ(os.str(), "1");
    delete n;
}

TEST(TestVariable, DerivationOther) {
    Variable v{"x"};
    std::ostringstream os;
    Expression *n = v.derive("y");
    os << *n;
    EXPECT_EQ(os.str(), "0");
    delete n;
}

TEST(TestVariable, TestInstanceCount) {
    Variable v{"x"};
    EXPECT_EQ(v.get_nb_instance(), 1);
}

TEST(TestNombre, TestIntanciation) {
    Nombre n{1};
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "1");
}

TEST(TestNombre, TestDerivation) {
    Nombre n{1};
    std::ostringstream os;
    Expression *n2 = n.derive("x");
    os << *n2;
    EXPECT_EQ(os.str(), "0");
    delete n2;
}

TEST(TestNombre, TestInstanceCount) {
    Nombre n{1};
    EXPECT_EQ(n.get_nb_instance(), 1);
}

TEST(TestAddition, PrintAddition) {
    Addition addition{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << addition;
    EXPECT_EQ(os.str(), "1 + x");
}

TEST(TestAddition, AdditionDerivation) {
    Addition addition{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << *addition.derive("x");
    EXPECT_EQ(os.str(), "0 + 1");
}

TEST(TestMultplication, PrintMultiplication) {
    Multiplication multiplication{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << multiplication;
    EXPECT_EQ(os.str(), "1 * x");
}

TEST(TestMultiplication, MultiplicationDerivation) {
    Multiplication multiplication{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << *multiplication.derive("x");
    EXPECT_EQ(os.str(), "1 * 1 + 0 * x");
}

TEST(TestSimplification, Addition1) {
    Addition addition{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << *addition.simplify();
    EXPECT_EQ(os.str(), "1 + x");
}

TEST(TestSimplification, Addition2) {
    Addition addition{new Nombre{0}, new Variable{"x"}};
    std::ostringstream os;
    os << *addition.simplify();
    EXPECT_EQ(os.str(), "x");
}

TEST(TestSimplification, Mulitplication1) {
    Multiplication multiplication{new Nombre{2}, new Variable{"x"}};
    std::ostringstream os;
    os << *multiplication.simplify();
    EXPECT_EQ(os.str(), "2 * x");
}

TEST(TestSimplification, Multiplication2) {
    Multiplication multiplication{new Nombre{1}, new Variable{"x"}};
    std::ostringstream os;
    os << *multiplication.simplify();
    EXPECT_EQ(os.str(), "x");
}

TEST(TestSimplification, Multiplication3) {
    Multiplication multiplication{new Nombre{0}, new Variable{"x"}};
    std::ostringstream os;
    os << *multiplication.simplify();
    EXPECT_EQ(os.str(), "0");
}


int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
